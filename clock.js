var secondHand = document.querySelector('.second-hand');
var minHand = document.querySelector('.min-hand');
var hourHand = document.querySelector('.hour-hand');

function setDate() {
    var now = new Date();

    var seconds = now.getSeconds();
    var secondsDegrees = ((seconds / 60) * 360) + 90;

    var minutes = now.getMinutes();
    var minutesDegrees = ((minutes / 60) * 360) + 90;

    var hours = now.getHours();
    var hoursDegrees = ((hours / 12) * 360) + 90;

    secondHand.style.transform = 'rotate('+secondsDegrees+'deg)';
    minHand.style.transform = 'rotate('+minutesDegrees+'deg)';
    hourHand.style.transform = 'rotate('+hoursDegrees+'deg)';
}

setInterval(setDate, 1000);